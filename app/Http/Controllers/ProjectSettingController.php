<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectSetting;

class ProjectSettingController extends Controller
{
    public function projectSettingsAll()
    {
        $settings = ProjectSetting::all();
        return response()->json(["settings"=>$settings]);
    }
	public function interview(Request $request)
	{
		if($request->post()) {
			return $request;
		} else {
			return view('admin.project-settings.interview');
		}
	}
    public function statusChange(Request $request)
    {
        $setting = ProjectSetting::find($request->data_id);
        if($setting) {
        	$setting->update([
        		'is_active' => $request->is_active
        	]);
        	return response()->json(["success"=>true]);
        } else {
        	return response()->json(["success"=>false]);
        }
    }

    public function addOrUpdate(Request $request)
    {
        //validate data
        $validator = \Validator::make($request->setting, [
            'key'=>'required|string',
            'value'=>'required|string',
            'is_active'=>'required|boolean',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' =>false , 'errors'=>$validator->messages()]);
        }

        if($request->setting['id']) {
            //update
            $setting = ProjectSetting::find($request->setting['id']);
            $setting->update([
                'key'=>$request->setting['key'],
                'value'=>$request->setting['value'],
                'is_active'=>$request->setting['is_active'],
            ]);
            $setting = ProjectSetting::find($request->setting['id']);
            return response()->json(["success"=>true, 'status'=>'updated', 'setting'=>$setting]);

        } else {
            // create
            $setting = ProjectSetting::where('key', '=', $request->setting['key'])->first();

            if ($setting) {
                return response()->json(['success' =>false , 'errors'=>['key_exists'=>['The key you entered is exists.']]]);
            }

            $setting = ProjectSetting::create([
                'key'=>$request->setting['key'],
                'value'=>$request->setting['value'],
                'is_active'=>$request->setting['is_active'],
            ]);
            return response()->json(["success"=>true, 'status'=>'created', 'setting'=>$setting]);
        }
    }
}
