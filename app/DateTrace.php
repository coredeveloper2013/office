<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateTrace extends Model
{
	protected $fillable = [
        'user_id', 'date', 'application_id', 'created_by', 'status', 'is_holiday', 'note'
    ];
}
